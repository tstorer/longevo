@include('messages')

<div class="row">
    <div class="col-md-offset-2 col-md-8">
        {!! Form::open(['route' => $route, 'method' => $method, 'class' => 'form-horizontal']) !!}
          <div class="form-group">
            <label for="order[client][email]" class="col-sm-2 control-label">Email (Cliente)</label>
            <div class="col-sm-10">
              {{ Form::text('order[client][email]', old('order.client.email', $order->client ? $order->client->email : ''), ['class' => 'form-control', 'placeholder' => 'Email']) }}
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
              <button type="submit" class="btn btn-default">Salvar</button>
            </div>
          </div>
        {!! Form::close() !!}
    </div>
</div>
