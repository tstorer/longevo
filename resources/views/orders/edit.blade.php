@extends('layouts.app')

@section('content')

<h1>Editar pedido #{{ $order->getKey() }}</h1>

@include('orders.form.form', ['route' => ['pedidos.update', $order->getKey()], 'method' => 'PUT'])

@endsection
