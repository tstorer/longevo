@extends('layouts.app')

@section('content')

<h1>Criar pedido</h1>

@include('orders.form.form', ['route' => ['pedidos.store'], 'method' => 'POST'])

@endsection
