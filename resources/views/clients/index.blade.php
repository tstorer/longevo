@extends('layouts.app')

@section('content')

@include('messages')

<div class="row">
    <div class="col-sm-offset-2 col-md-8">
        {!! Form::open(['route' => ['clientes.index'], 'method' => 'GET', 'class' => 'form-horizontal']) !!}
            <div class="form-group">
                <div class="col-sm-5">
                    <label for="name" class="control-label">Nome</label>
                    {{ Form::text('name', request('name'), ['class' => 'form-control', 'placeholder' => 'Nome']) }}
                </div>
                <div class="col-sm-5">
                    <label for="email" class="control-label">Email</label>
                    {{ Form::text('email', request('email'), ['class' => 'form-control', 'placeholder' => 'Email']) }}
                </div>
                <div class="col-sm-2">
                    <button type="submit" class="btn btn-default">Buscar</button>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
    <div class="col-md-2">
        <a class="btn" href="{{ action('ClientController@create') }}">Novo cliente</a>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-sm-offset-2 col-md-8">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nome</th>
                    <th>Email</th>
                    <th>Última atualização</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($clients as $client)
                <tr>
                    <td><a href="{{ route('clientes.edit', $client->getKey()) }}">{{ $client->getKey() }}</a></td>
                    <td>{{ $client->name }}</td>
                    <td>{{ $client->email }}</td>
                    <td>{{ Carbon\Carbon::parse($client->updated_at)->format('d/m/Y H:i:s') }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>

<div class="row">
    <div class="col-sm-offset-2 col-md-8">
        {{ $clients->links() }}
    </div>
</div>

@endsection
