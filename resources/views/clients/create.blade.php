@extends('layouts.app')

@section('content')

<h1>Criar cliente</h1>

@include('clients.form.form', ['route' => ['clientes.store'], 'method' => 'POST'])

@endsection
