@extends('layouts.app')

@section('content')

<h1>Editar cliente #{{ $client->getKey() }}</h1>

@include('clients.form.form', ['route' => ['clientes.update', $client->getKey()], 'method' => 'PUT'])

@endsection
