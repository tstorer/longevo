@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="center-block">Teste Longevo</div>
        <hr>
        <div class="list-group">
          <a href="{{ action('ClientController@index') }}" class="list-group-item">Clientes <span class="badge">{{ App\Models\Client::count() }}</span></a>
          <a href="{{ action('OrderController@index') }}" class="list-group-item">Pedidos <span class="badge">{{ App\Models\Order::count() }}</span></a>
          <a href="{{ action('TicketController@index') }}" class="list-group-item">Chamados <span class="badge">{{ App\Models\Ticket::count() }}</span></a>
        </div>
    </div>
</div>

@endsection
