<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>{{ $title or 'Longevo' }}</title>

<!-- Global stylesheets -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<!-- /global stylesheets -->

<!-- Core JS files -->
<script type="text/javascript" src="/js/core/libraries/jquery.min.js"></script>
<script type="text/javascript" src="/js/core/libraries/bootstrap.min.js"></script>
<!-- /core JS files -->
