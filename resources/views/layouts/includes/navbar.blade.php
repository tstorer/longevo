<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="@if (request()->is('/')) active @endif"><a href="{{ action('IndexController@index') }}">Home</a></li>
        <li class="@if (request()->is('clientes*')) active @endif"><a href="{{ action('ClientController@index') }}">Clientes</a></li>
        <li class="@if (request()->is('pedidos*')) active @endif"><a href="{{ action('OrderController@index') }}">Pedidos</a></li>
        <li class="@if (request()->is('chamados*')) active @endif"><a href="{{ action('TicketController@index') }}">Chamados</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
