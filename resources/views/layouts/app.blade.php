<!DOCTYPE html>
<html lang="en">
<head>
    @include('layouts.includes.header')
</head>
<body id="app-layout">
    <div class="page-navbar">
        @include('layouts.includes.navbar')
    </div>

    <div class="container">
        @yield('content')
    </div>

    @include('layouts.includes.footer')
</body>
</html>
