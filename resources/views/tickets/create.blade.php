@extends('layouts.app')

@section('content')

<h1>Criar chamado</h1>

@include('tickets.form.form', ['route' => ['chamados.store'], 'method' => 'POST'])

@endsection
