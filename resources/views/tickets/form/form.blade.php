@include('messages')

<div class="row">
    <div class="col-md-offset-2 col-md-8">
        {!! Form::open(['route' => $route, 'method' => $method, 'class' => 'form-horizontal']) !!}
          <div class="form-group">
            <label for="ticket[client][name]" class="col-sm-2 control-label">Nome</label>
            <div class="col-sm-10">
              {{ Form::text('ticket[client][name]', old('ticket.client.name', $ticket->order ? $ticket->order->client->name : ''), ['class' => 'form-control', 'placeholder' => 'Nome']) }}
            </div>
          </div>
          <div class="form-group">
            <label for="ticket[client][email]" class="col-sm-2 control-label">Email</label>
            <div class="col-sm-10">
             {{ Form::text('ticket[client][email]', old('ticket.client.email', $ticket->order ? $ticket->order->client->email : ''), ['class' => 'form-control', 'placeholder' => 'Email']) }}
            </div>
          </div>
          <div class="form-group">
            <label for="ticket[order][id]" class="col-sm-2 control-label">Pedido</label>
            <div class="col-sm-10">
             {{ Form::number('ticket[order][id]', old('ticket.order.id', $ticket->order ? $ticket->order->getKey() : ''), ['class' => 'form-control', 'placeholder' => 'Pedido', 'min' => 0]) }}
            </div>
          </div>
          <div class="form-group">
            <label for="ticket[title]" class="col-sm-2 control-label">Título</label>
            <div class="col-sm-10">
             {{ Form::text('ticket[title]', old('ticket.title', $ticket->title), ['class' => 'form-control', 'placeholder' => 'Título']) }}
            </div>
          </div>
          <div class="form-group">
            <label for="ticket[description]" class="col-sm-2 control-label">Observações</label>
            <div class="col-sm-10">
             {{ Form::textarea('ticket[description]', old('ticket.description', $ticket->description), ['class' => 'form-control', 'placeholder' => 'Observações']) }}
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
              <button type="submit" class="btn btn-default">Salvar</button>
            </div>
          </div>
        {!! Form::close() !!}
    </div>
</div>
