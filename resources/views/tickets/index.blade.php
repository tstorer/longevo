@extends('layouts.app')

@section('content')

@include('messages')

<div class="row">
    <div class="col-sm-offset-2 col-md-8">
        {!! Form::open(['route' => ['chamados.index'], 'method' => 'GET', 'class' => 'form-horizontal']) !!}
            <div class="form-group">
                <div class="col-sm-5">
                    <label for="email" class="control-label">Email (Cliente)</label>
                    {{ Form::text('email', request('email'), ['class' => 'form-control', 'placeholder' => 'Email']) }}
                </div>
                <div class="col-sm-5">
                    <label for="order" class="control-label">Pedido</label>
                    {{ Form::number('order', request('order'), ['class' => 'form-control', 'placeholder' => 'Pedido', 'min' => 0]) }}
                </div>
                <div class="col-sm-2">
                    <button type="submit" class="btn btn-default">Buscar</button>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
    <div class="col-md-2">
        <a class="btn" href="{{ action('TicketController@create') }}">Novo chamado</a>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-sm-offset-2 col-md-8">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nome</th>
                    <th>Email</th>
                    <th>Pedido</th>
                    <th>Titulo</th>
                    <th>Última atualização</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($tickets as $ticket)
                <tr>
                    <td><a href="{{ route('chamados.edit', $ticket->getKey()) }}">{{ $ticket->getKey() }}</a></td>
                    <td>{{ $ticket->order->client->name }}</td>
                    <td>{{ $ticket->order->client->email }}</td>
                    <td>{{ $ticket->order->getKey() }}</td>
                    <td>{{ $ticket->title }}</td>
                    <td>{{ Carbon\Carbon::parse($ticket->updated_at)->format('d/m/Y H:i:s') }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>

<div class="row">
    <div class="col-sm-offset-2 col-md-8">
        {{ $tickets->links() }}
    </div>
</div>

@endsection
