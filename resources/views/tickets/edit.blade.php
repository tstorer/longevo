@extends('layouts.app')

@section('content')

<h1>Editar chamado #{{ $ticket->getKey() }}</h1>

@include('tickets.form.form', ['route' => ['chamados.update', $ticket->getKey()], 'method' => 'PUT'])

@endsection
