<?php

namespace App\Flows\SaveFlows;

use App\Models\Client;
use App\Models\Order;
use App\Models\Ticket;
use Validator;

class TicketSaveFlow implements SaveFlow
{
    protected $ticket;

    protected $order;

    protected $client;

    public function __construct(Ticket $ticket, Order $order, Client $client)
    {
        $this->ticket = $ticket;
        $this->order = $order;
        $this->client = $client;
    }

    public function validate()
    {
        Validator::make($this->order->getAttributes(), [
            'id' => 'exists:orders',
        ])->validate();
    }

    public function save()
    {
        if (!$this->order->getKey() || !$this->client->getKey()) {
            $orderSaveFlow = new OrderSaveFlow($this->order, $this->client);
            $orderSaveFlow->validate();
            $orderSaveFlow->save();
        }

        $this->ticket->order()->associate($this->order);
        $this->ticket->save();
    }
}
