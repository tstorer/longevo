<?php

namespace App\Flows\SaveFlows;

use App\Models\Client;
use Validator;

class ClientSaveFlow implements SaveFlow
{
    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function validate()
    {
        Validator::make($this->client->getAttributes(), [
            'name' => 'max:255',
            'email' => 'required|email|unique:clients',
        ])->validate();
    }

    public function save()
    {
        $this->client->save();
    }
}
