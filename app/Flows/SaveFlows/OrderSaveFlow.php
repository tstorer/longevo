<?php

namespace App\Flows\SaveFlows;

use App\Models\Client;
use App\Models\Order;

class OrderSaveFlow implements SaveFlow
{
    protected $order;

    protected $client;

    public function __construct(Order $order, Client $client)
    {
        $this->order = $order;
        $this->client = $client;
    }

    public function validate()
    {
        return true;
    }

    public function save()
    {
        if (!$this->client->getKey()) {
            $clientSaveFlow = new ClientSaveFlow($this->client);
            $clientSaveFlow->validate();
            $clientSaveFlow->save();
        }

        $this->order->client()->associate($this->client);
        $this->order->save();
    }
}
