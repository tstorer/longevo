<?php

namespace App\Flows\SaveFlows;

interface SaveFlow
{
    public function validate();

    public function save();
}
