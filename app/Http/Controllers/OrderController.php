<?php

namespace App\Http\Controllers;

use App\Flows\SaveFlows\OrderSaveFlow;
use App\Models\Client;
use App\Models\Order;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Redirect;
use View;

class OrderController extends Controller
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = $this->createQueryString();

        return View::make('orders.index')
            ->withOrders($orders->paginate(self::DEFAULT_PAGINATION));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $order = new Order();

        return View::make('orders.create')
            ->withOrder($order);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $order = new Order();
        $orderSaveFlow = new OrderSaveFlow($order, Client::firstOrNew($this->request->input('order.client')));
        $orderSaveFlow->validate();

        try {
            $orderSaveFlow->save();

            return Redirect::route('pedidos.index')->with('success', 'Pedido ' . $order->getKey() . ' criado com sucesso');
        } catch (QueryException $e) {
            return Redirect::back()->withErrors([$e->getMessage()]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($orderId)
    {
        $order = Order::find($orderId);
        if (!$order) {
            return Redirect::back()->withErrors(['Pedido não encontrado']);
        }

        return View::make('orders.edit')
            ->withOrder($order);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $orderId)
    {
        $order = Order::find($orderId);
        if (!$order) {
            return Redirect::back()->withErrors(['Pedido não encontrado']);
        }

        $orderSaveFlow = new OrderSaveFlow($order, Client::firstOrNew($this->request->input('order.client')));
        $orderSaveFlow->validate();

        try {
            $orderSaveFlow->save();

            return Redirect::route('pedidos.index')->with('success', 'Pedido ' . $order->getKey() . ' atualizado com sucesso');
        } catch (QueryException $e) {
            return Redirect::back()->withErrors([$e->getMessage()]);
        }
    }

    protected function createQueryString()
    {
        $orders = Order::query();

        if ($this->request->input('email')) {
            $orders->whereHas('client', function ($q) {
                $q->where('email', 'like', '%' . $this->request->input('email') . '%');
            });
        }

        if ($this->request->input('order')) {
            $orders->where('id', '=', $this->request->input('order'));
        }

        return $orders;
    }
}
