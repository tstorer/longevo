<?php

namespace App\Http\Controllers;

use App\Flows\SaveFlows\ClientSaveFlow;
use App\Models\Client;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Redirect;
use View;

class ClientController extends Controller
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = $this->createQueryString();

        return View::make('clients.index')
            ->withClients($clients->paginate(self::DEFAULT_PAGINATION));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $client = new Client();

        return View::make('clients.create')
            ->withClient($client);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $client = (new Client())->fill($this->request->input('client'));
        $clientSaveFlow = new ClientSaveFlow($client);
        $clientSaveFlow->validate();

        try {
            $clientSaveFlow->save();

            return Redirect::route('clientes.index')->with('success', 'Cliente ' . $client->email . ' criado com sucesso');
        } catch (QueryException $e) {
            return Redirect::back()->withErrors([$e->getMessage()]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($clientId)
    {
        $client = Client::find($clientId);
        if (!$client) {
            return Redirect::back()->withErrors(['Cliente não encontrado']);
        }

        return View::make('clients.edit')
            ->withClient($client);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($clientId)
    {
        $client = Client::find($clientId);
        if (!$client) {
            return Redirect::back()->withErrors(['Cliente não encontrado']);
        }

        $client->fill($this->request->input('client'));
        $clientSaveFlow = new ClientSaveFlow($client);

        try {
            $clientSaveFlow->save();

            return Redirect::route('clientes.index')->with('success', 'Cliente ' . $client->email . ' atualizado com sucesso');
        } catch (QueryException $e) {
            return Redirect::back()->withErrors([$e->getMessage()]);
        }
    }

    protected function createQueryString()
    {
        $clients = Client::query();

        if ($this->request->input('name')) {
            $clients->where('name', 'like', '%' . $this->request->input('name') . '%');
        }

        if ($this->request->input('email')) {
            $clients->where('email', 'like', '%' . $this->request->input('email') . '%');
        }

        return $clients;
    }
}
