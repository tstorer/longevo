<?php

namespace App\Http\Controllers;

use App\Flows\SaveFlows\TicketSaveFlow;
use App\Models\Client;
use App\Models\Order;
use App\Models\Ticket;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Redirect;
use View;

class TicketController extends Controller
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tickets = $this->createQueryString();

        return View::make('tickets.index')
            ->withTickets($tickets->paginate(self::DEFAULT_PAGINATION));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ticket = new Ticket();

        return View::make('tickets.create')
            ->withTicket($ticket);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ticket = (new Ticket())->fill($this->request->input('ticket'));
        $ticketSaveFlow = new TicketSaveFlow(
            $ticket,
            Order::firstOrNew($this->request->input('ticket.order')),
            Client::firstOrNew($this->request->input('ticket.client'))
        );
        $ticketSaveFlow->validate();

        try {
            $ticketSaveFlow->save();

            return Redirect::route('chamados.index')->with('success', 'Chamado ' . $ticket->getKey() . ' criado com sucesso');
        } catch (QueryException $e) {
            return Redirect::back()->withErrors([$e->getMessage()]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($ticketId)
    {
        $ticket = Ticket::find($ticketId);
        if (!$ticket) {
            return Redirect::back()->withErrors(['Chamado não encontrado']);
        }

        return View::make('tickets.edit')
            ->withTicket($ticket);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $ticketId)
    {
        $ticket = Ticket::find($ticketId);
        if (!$ticket) {
            return Redirect::back()->withErrors(['Chamado não encontrado']);
        }

        $ticket->fill($this->request->input('ticket'));
        $ticketSaveFlow = new TicketSaveFlow(
            $ticket,
            Order::firstOrNew($this->request->input('ticket.order')),
            Client::firstOrNew($this->request->input('ticket.client'))
        );
        $ticketSaveFlow->validate();

        try {
            $ticketSaveFlow->save();

            return Redirect::route('chamados.index')->with('success', 'Chamado ' . $ticket->getKey() . ' atualizado com sucesso');
        } catch (QueryException $e) {
            return Redirect::back()->withErrors([$e->getMessage()]);
        }
    }

    protected function createQueryString()
    {
        $tickets = Ticket::query();

        if ($this->request->input('email')) {
            $tickets->whereHas('order.client', function ($q) {
                $q->where('email', 'like', '%' . $this->request->input('email') . '%');
            });
        }

        if ($this->request->input('order')) {
            $tickets->whereHas('order', function ($q) {
                $q->where('id', '=', $this->request->input('order'));
            });
        }

        return $tickets;
    }
}
