<?php

namespace App\Models;

class Order extends Model
{
    protected $fillable = ['id', 'client_id'];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }
}
