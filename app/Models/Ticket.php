<?php

namespace App\Models;

class Ticket extends Model
{
    protected $fillable = [
        'order_id',
        'title',
        'description',
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
