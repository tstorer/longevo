<?php

use App\Models\Client;
use Illuminate\Database\Seeder;

class ClientTableSeeder extends Seeder
{
    protected $clients = [
        0 => [
            'name' => 'Cliente 1',
            'email' => 'client_1@test.com',
        ],
        1 => [
            'name' => 'Cliente 2',
            'email' => 'client_2@test.com',
        ],
        2 => [
            'name' => 'Cliente 3',
            'email' => 'client_3@test.com',
        ],
        3 => [
            'name' => 'Cliente 4',
            'email' => 'client_4@test.com',
        ],
        4 => [
            'name' => 'Cliente 5',
            'email' => 'client_5@test.com',
        ],
        5 => [
            'name' => 'Cliente 6',
            'email' => 'client_6@test.com',
        ],
        6 => [
            'name' => 'Cliente 7',
            'email' => 'client_7@test.com',
        ],
        7 => [
            'name' => 'Cliente 8',
            'email' => 'client_8@test.com',
        ],
        8 => [
            'name' => 'Cliente 9',
            'email' => 'client_9@test.com',
        ],
        9 => [
            'name' => 'Cliente 10',
            'email' => 'client_10@test.com',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clients')->truncate();
        foreach ($this->clients as $client) {
            (new Client($client))->save();
        }
    }
}
