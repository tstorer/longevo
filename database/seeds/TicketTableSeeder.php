<?php

use App\Models\Ticket;
use Illuminate\Database\Seeder;

class TicketTableSeeder extends Seeder
{
    protected $tickets = [
        0 => [
            'order_id' => 1,
            'title' => 'Chamado 1',
            'description' => 'Descrição chamado 1',
        ],
        1 => [
            'order_id' => 2,
            'title' => 'Chamado 2',
            'description' => 'Descrição chamado 2',
        ],
        2 => [
            'order_id' => 3,
            'title' => 'Chamado 3',
            'description' => 'Descrição chamado 3',
        ],
        3 => [
            'order_id' => 4,
            'title' => 'Chamado 4',
            'description' => 'Descrição chamado 4',
        ],
        4 => [
            'order_id' => 5,
            'title' => 'Chamado 5',
            'description' => 'Descrição chamado 5',
        ],
        5 => [
            'order_id' => 6,
            'title' => 'Chamado 6',
            'description' => 'Descrição chamado 6',
        ],
        6 => [
            'order_id' => 7,
            'title' => 'Chamado 7',
            'description' => 'Descrição chamado 7',
        ],
        7 => [
            'order_id' => 8,
            'title' => 'Chamado 8',
            'description' => 'Descrição chamado 8',
        ],
        8 => [
            'order_id' => 9,
            'title' => 'Chamado 9',
            'description' => 'Descrição chamado 9',
        ],
        9 => [
            'order_id' => 10,
            'title' => 'Chamado 10',
            'description' => 'Descrição chamado 10',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tickets')->truncate();
        foreach ($this->tickets as $ticket) {
            (new Ticket($ticket))->save();
        }
    }
}
