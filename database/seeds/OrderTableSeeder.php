<?php

use App\Models\Order;
use Illuminate\Database\Seeder;

class OrderTableSeeder extends Seeder
{
    protected $orders = [
        0 => [
            'client_id' => 1,
        ],
        1 => [
            'client_id' => 2,
        ],
        2 => [
            'client_id' => 3,
        ],
        3 => [
            'client_id' => 4,
        ],
        4 => [
            'client_id' => 5,
        ],
        5 => [
            'client_id' => 6,
        ],
        6 => [
            'client_id' => 7,
        ],
        7 => [
            'client_id' => 8,
        ],
        8 => [
            'client_id' => 9,
        ],
        9 => [
            'client_id' => 10,
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders')->truncate();
        foreach ($this->orders as $order) {
            (new Order($order))->save();
        }
    }
}
