Teste Longevo

Após clonar o projeto inserir as credenciais de acesso ao banco de dados. As variáveis estão localizadas no arquivo .env (DB_DATABASE, DB_USERNAME, DB_PASSWORD)

A partir do diretório raiz do projeto executar os seguintes comandos:

1. composer install
2. php artisan serve

O projeto podera ser acessado através da porta 8000.
